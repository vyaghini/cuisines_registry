package de.quandoo.recruitment.registry;

import com.google.common.base.Preconditions;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.min;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    /**
        Cuisine and Customer seem to have n-to-n relationship so one way would be
        to maintain both directions of the association separately in two maps
     */
    private Map<Customer , Set<Cuisine>> customerToCuisineMap = new HashMap<>();
    private Map<Cuisine , Set<Customer>> cuisineToCustomerMap = new HashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {

        Preconditions.checkArgument(customer!=null, "Customer cannot be null");
        Preconditions.checkArgument(cuisine!=null, "Cuisine cannot be null");

        cuisineToCustomerMap.computeIfAbsent(cuisine, k -> new HashSet<>());
        cuisineToCustomerMap.get(cuisine).add(customer);

        customerToCuisineMap.computeIfAbsent(customer, k-> new HashSet<>());
        customerToCuisineMap.get(customer).add(cuisine);

    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisineToCustomerMap.get(cuisine)==null){
            return new ArrayList<>();
        }

        return new ArrayList<>(cuisineToCustomerMap.get(cuisine));
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customerToCuisineMap.get(customer) == null) {
            return new ArrayList<>();
        }

        return new ArrayList<>(customerToCuisineMap.get(customer));
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {

        Preconditions.checkArgument(n>=1, "Number of top cuisines queried should be equal or greater than 1");

        List<Cuisine> cuisinesSortedByCustomerSize = cuisineToCustomerMap
                .entrySet()
                .stream()
                .sorted(Comparator.comparingInt((Map.Entry<Cuisine,Set<Customer>> e) -> e.getValue().size()).reversed())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return cuisinesSortedByCustomerSize.subList(0, min(n, cuisinesSortedByCustomerSize.size()));
    }
}

