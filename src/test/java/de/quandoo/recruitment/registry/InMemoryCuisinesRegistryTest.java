package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    @Test  (expected = IllegalArgumentException.class)
    public void register_registerNullCustomer_throwsException() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(null, new Cuisine("french"));
    }

    @Test  (expected = IllegalArgumentException.class)
    public void register_registerNullCuisine_throwsException() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), null);
    }


    @Test
    public void cuisineCustomers_registerValidValues_returnsRightValues() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        assertEquals( new Customer("1") , cuisinesRegistry.cuisineCustomers(new Cuisine("french")).get(0));
        assertEquals( new Customer("2") , cuisinesRegistry.cuisineCustomers(new Cuisine("german")).get(0));
        assertEquals( new Customer("3") , cuisinesRegistry.cuisineCustomers(new Cuisine("italian")).get(0));
    }

    @Test
    public void cuisineCustomers_queryNonRegisteredCuisine_returnsEmptyList() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        assertTrue(cuisinesRegistry.cuisineCustomers( new Cuisine("russian")).isEmpty());
    }

    @Test
    public void customerCuisines_queryNullCustomer_returnsEmptyList() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        assertTrue(cuisinesRegistry.customerCuisines(null).isEmpty());
    }


    @Test
    public void customerCuisines_registerValues_returnsRightValues() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        assertEquals( new Cuisine("french") , cuisinesRegistry.customerCuisines(new Customer("1")).get(0));
        assertEquals( new Cuisine("german") , cuisinesRegistry.customerCuisines(new Customer("2")).get(0));
        assertEquals( new Cuisine("italian") , cuisinesRegistry.customerCuisines(new Customer("3")).get(0));

    }

    @Test
    public void customerCuisines_queryNonRegisteredCustomer_returnsEmptyList() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        assertTrue(cuisinesRegistry.customerCuisines( new Customer("1234")).isEmpty());

    }

    @Test
    public void cuisineCustomers_queryNullCuisine_returnsEmptyList() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        assertTrue(cuisinesRegistry.cuisineCustomers(null).isEmpty());
    }

    @Test
    public void topCuisines_cuisinesWithDifferentNumberOfCustomersRegistered_returnsTopTwoCorrectly() {

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("6"), new Cuisine("italian"));

        List<Cuisine> top2Cuisines = cuisinesRegistry.topCuisines(2);

        assertEquals(2, top2Cuisines.size());
        assertEquals(new Cuisine("italian") , top2Cuisines.get(0));
        assertEquals(new Cuisine("german") , top2Cuisines.get(1));
    }

    @Test
    public void topCuisines_3CuisinesWithDifferentNumberOfCustomersRegisteredTop5Queried_returns3() { // n > cuisines.length

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("6"), new Cuisine("italian"));

        List<Cuisine> top2Cuisines = cuisinesRegistry.topCuisines(5);

        assertEquals(3, top2Cuisines.size());

    }


    @Test (expected = IllegalArgumentException.class)
    public void topCuisines_zeroParam_throwsException() { // n > cuisines.length

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));

        cuisinesRegistry.topCuisines(0);

    }

    @Test (expected = IllegalArgumentException.class)
    public void topCuisines_minusParam_throwsException() { // n > cuisines.length

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));

        cuisinesRegistry.topCuisines(-2);

    }

    @Test
    public void topCuisines_emptyRegistry_returnsEmptyResult() { // n > cuisines.length

        InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

        assertTrue(cuisinesRegistry.topCuisines(2).isEmpty());

    }

}